#!/bin/bash -e

version=1.0.6 

rm consul || true
rm consul_${version}_linux_amd64.zip || true

wget -q  https://releases.hashicorp.com/consul/${version}/consul_${version}_linux_amd64.zip
unzip consul_${version}_linux_amd64.zip

./consul agent -dev -config-file=0def.json > consul10.log &
echo "Sleeping 5 for consul to come up"
sleep 3
./consul kv put infra/pagerduty/default/key 12341234123412341234123412341234
./consul kv put infra/pagerduty/queue/DeadPastService 1234512345
sleep 1

cat single.json | ../consul2pd |& tee consul10_pd1.log &
cat single.json | ../consul2pd |& tee consul10_pd2.log


echo "Shutting down the server"
./consul kv put infra/pagerduty/byemessage "good night"

sleep 10

kill %1

### Testing on the old version

version=0.7.5

rm consul || true
rm consul_${version}_linux_amd64.zip || true

wget -q  https://releases.hashicorp.com/consul/${version}/consul_${version}_linux_amd64.zip
unzip consul_${version}_linux_amd64.zip

./consul agent -dev -config-file=0def.json > consul07.log &
echo "Sleeping 5 for consul to come up"
sleep 3
./consul kv put infra/pagerduty/default/key 12341234123412341234123412341234
./consul kv put infra/pagerduty/queue/DeadPastService 1234512345
sleep 1

cat single.json | ../consul2pd 2> consul07_pd1.log &
cat single.json | ../consul2pd 2> consul07_pd2.log


echo "Shutting down the server"
./consul kv put infra/pagerduty/byemessage "good night"

sleep 1

jobs -l
kill %% || true

echo "Done"

