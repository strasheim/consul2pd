package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"log/syslog"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/hashicorp/consul/api"
)

// o for options, lazy global struct var
var (
	o struct {
		apiKey  string
		client  *api.Client
		proxy   string
		kv      *api.KV
		twoMago int64
		healthy bool
		lock    *api.Lock
		version string
	}
)

// Setting the log ulEvents watchEventtream, creating the consul API client
// Setting the proxy if needed and getting a sense of the time
func init() {
	logwriter, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err == nil {
		log.SetFlags(0)
		log.SetOutput(logwriter)
	}
	config := api.DefaultConfig()
	if len(os.Getenv("TOKEN")) > 1 {
		config.Token = os.Getenv("TOKEN")
	}
	o.client, err = api.NewClient(config)
	check(err)

	o.healthy, o.version = assumeHealth()

	opts := &api.LockOptions{
		Key:          "infra/pagerduty/sprawl",
		SessionTTL:   "10s",
		LockWaitTime: (1 * time.Millisecond),
		LockTryOnce:  true,
	}
	o.lock, err = o.client.LockOpts(opts)
	check(err)

	o.kv = o.client.KV()
	proxy, err := readProxy()
	if err == nil {
		os.Setenv("HTTPS_PROXY", proxy)
	}

}

func main() {
	maintainance() // Aborts with exit 0 if set

	var pagerEvents []EventRequest
	consulEvents := make(watchEvent, 0)
	bytes, _ := ioutil.ReadAll(os.Stdin)
	err := json.Unmarshal(bytes, &consulEvents)
	check(err)

	if o.healthy {
		addToQueue(consulEvents)
		lockCh, err := o.lock.Lock(nil)
		check(err)

		if lockCh != nil {
			// Leader which will send in a few seconds event to PD
			time.Sleep(5 * time.Second)
			err = o.lock.Unlock()
			check(err)
			findOldEvents(&pagerEvents)
			log.Println("Consul 2 Pagerduty -> Number of stored events to submit:", len(pagerEvents))
		}
	} else { // unhealthy and not version 1.0.0
		if o.version == "0.7.5" {
			findOldEvents(&pagerEvents)
		}
		decorateEvent(consulEvents, &pagerEvents)
	}
	//log.Println("Consul 2 Pagerduty -> Number of new events to submit:", len(pagerEvents))

	for _, v := range pagerEvents {
		service, check, err := v.submit()
		if err != nil {
			if len(service) == 0 {
				log.Println("Service without serviceID", check, err)
			} else {
				storeEvent(service)
			}
		} else {
			log.Println("Submitted", service, check)
		}
	}
}

// Converting the consul event struct into the pagerduty event struct
// Reporting on per service base - not per check
func decorateEvent(consulEvents watchEvent, pagerEvents *[]EventRequest) {

	// Creating the generic details: Consul's complete overview of the service
	// Including details of all checks belong to the class / group of the service
	m := make(map[string][]string, 0)
	for _, e := range consulEvents {
		for _, sp := range e.Checks {
			if sp.Node == "at_run_time" {
				sp.Node, _ = os.Hostname()
			}
			if len(sp.ServiceName) == 0 {
				sp.ServiceName = sp.Name
			}
			m[e.Service.ID] = append(m[e.Service.ID], strings.Trim(sp.Node+" "+sp.ServiceName+" "+sp.CheckID+" "+sp.Status+" "+sp.Output, "\n"))
		}
	}

	for _, v := range consulEvents {
		critical := 10000000
		for i, l := range v.Checks {
			if l.Status == "critical" {
				critical = i
				break
			}
		}
		if critical != 10000000 {
			sp := v.Checks[critical]
			if sp.CheckID == "serfHealth" {
				sp.ServiceName = "ConsulAgent"
			}
			if len(sp.ServiceName) == 0 {
				sp.ServiceName = sp.Name
			}
			if sp.Node == "at_run_time" {
				sp.Node, _ = os.Hostname()
			}
			pagerdutyKey, err := readPdKey(sp.ServiceName, v.Service.Tags)
			check(err)
			log.Println("Received Critical Notification for", sp.Node, sp.ServiceName)
			event := PagerEvent(pagerdutyKey, sp.Node+"/"+v.Service.ID, "trigger")
			event.Payload.Summary = sp.Node + " " + v.Service.ID + " " + sp.Status
			event.Payload.Severity = "critical"
			event.Payload.Source = sp.Node
			event.Payload.CustomDetails = m
			event.Payload.Component = v.Service.ID
			event.Payload.Class = sp.CheckID
			event.Links = readPdLink(sp.ServiceName)
			*pagerEvents = append(*pagerEvents, *event)
		} else {
			sp := v.Checks[0]
			if sp.CheckID == "serfHealth" {
				sp.ServiceName = "ConsulAgent"
			}
			if len(sp.ServiceName) == 0 {
				sp.ServiceName = sp.Name
			}
			if sp.Node == "at_run_time" {
				sp.Node, _ = os.Hostname()
			}

			pagerdutyKey, err := readPdKey(sp.ServiceName, v.Service.Tags)
			check(err)
			event := PagerEvent(pagerdutyKey, sp.Node+"/"+v.Service.ID, "resolve")
			event.Payload.Summary = sp.Node + " " + v.Service.ID + " " + sp.Status
			event.Payload.Severity = "info"
			event.Payload.Source = sp.Node
			event.Payload.Component = v.Service.ID
			event.Payload.Class = sp.CheckID
			*pagerEvents = append(*pagerEvents, *event)
		}
	}

}

func readPdKey(service string, tags []string) (string, error) {
	for _, tag := range tags {
		if strings.HasPrefix(tag, "Pdkey:") {
			service = strings.Replace(tag, "Pdkey:", "", -1)
		}
	}
	kv := o.kv
	opts := api.QueryOptions{AllowStale: true}
	pair, _, err := kv.Get("infra/pagerduty/"+service+"/key", &opts)
	if err != nil {
		return "", err
	}
	if pair == nil {
		// Fallback to default key
		pair, _, err = kv.Get("infra/pagerduty/default/key", &opts)
		if err != nil {
			return "", err
		}
		if pair == nil {
			return "", errors.New("No PD API key found")
		}
	}
	if len(string(pair.Value)) != 32 {
		return "", errors.New("The PD API key needs to be 32 characters long")
	}
	return string(pair.Value), nil
}

func readPdLink(service string) Links {
	kv := o.kv
	opts := api.QueryOptions{AllowStale: true}
	pair, _, err := kv.Get("infra/pagerduty/"+service+"/links", &opts)
	if err != nil {
		return nil
	}
	if pair == nil {
		// Fallback to default Link
		pair, _, err = kv.Get("infra/pagerduty/default/links", &opts)
		if err != nil {
			return nil
		}
		if pair == nil {
			return nil
		}
	}
	// Here I have data
	data := strings.Split(string(pair.Value), "\n")
	links := make(Links, 0)
	singleLink := make(Links, 1)
	var url string
	for i, v := range data {
		if i%2 == 0 { // even
			url = v
		} else {
			singleLink[0].Href = url
			singleLink[0].Text = v
			links = append(links, singleLink...)
		}
	}
	/// needs to be a struct {
	return links
}

// Allowing stale query, meaning local resolution [ no leader needed ]
// If no error is reported the returned string is taken as proxy
func readProxy() (string, error) {
	kv := o.kv
	opts := api.QueryOptions{AllowStale: true}
	pair, _, err := kv.Get("infra/pagerdutyproxy", &opts)
	if err != nil {
		return "", err
	}
	if pair == nil {
		return "", errors.New("No proxy required")
	}
	return string(pair.Value), err
}

// Needs to put this into statsD as failure
func check(err error) {
	if err != nil {
		log.Println(err)
		os.Exit(2)
	}
}

// Uses the consul KV store to remember unsuccessful posts to PD
// On the next event the timestamp is checked and a retry is happening
func storeEvent(service string) {
	kv := o.kv
	opts := api.QueryOptions{AllowStale: true}
	pair, _, err := kv.Get("infra/pagerduty/queue/"+service, &opts)
	if err != nil {
		return
	}
	if pair == nil {
		log.Println("Added", service, "to the queue")
		t := time.Now().Unix()
		s := fmt.Sprintf("%d", t)
		p := &api.KVPair{Key: "infra/pagerduty/queue/" + service, Value: []byte(s)}
		_, err := kv.Put(p, nil)
		if err != nil {
			log.Println(err)
		}
	}
}

// Checking the consul KV store (locally, stale entries allowed ) for services that need to be retransmitted
// When a service is found, consul is asked for the current state of that service and that state is transmitted
func findOldEvents(pagerEvents *[]EventRequest) {
	kv := o.kv
	opts := api.QueryOptions{AllowStale: true}
	queue, _, err := kv.List("infra/pagerduty/queue/", &opts)
	if err != nil {
		log.Println(err)
		return
	}
	if len(queue) == 0 {
		return
	}

	for _, service := range queue {
		key := string(service.Key)
		key = strings.Replace(key, "infra/pagerduty/queue/", "", -1)
		// XXX Fix me if you can
		_, err := strconv.ParseInt((string(service.Value)), 10, 64)
		if err != nil {
			log.Println(err)
			continue
		}

		if len(key) == 0 {
			log.Println("Deleting empty service string")
			_, err = kv.Delete(string(service.Key), nil)
			if err != nil {
				log.Println(err)
			}
			continue
		}

		client := &http.Client{}
		req, _ := http.NewRequest("GET", "http://localhost:8500/v1/health/service/"+key, nil)
		if len(os.Getenv("TOKEN")) > 1 {
			req.Header.Set("X-Consul-Token", os.Getenv("TOKEN"))
		}
		response, err := client.Do(req)
		if err != nil {
			log.Println(err)
			continue
		}
		defer response.Body.Close()
		bytes, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Println(err)
			continue
		}
		consulEvents := make(watchEvent, 0)
		err = json.Unmarshal(bytes, &consulEvents)
		if err != nil {
			log.Println(err)
			continue
		}
		goodEntry := true
		for i, v := range consulEvents {
			if v.Service.Service != key {
				log.Println("#", i, "Service", v.Service.Service, "no longer exists, removing without submission")
				goodEntry = false
			}
			if len(v.Checks) == 0 {
				log.Println("#", i, "Service", v.Service.Service, "does not have checks, removing without submission")
				goodEntry = false
			}
		}
		if len(consulEvents) != 0 && goodEntry {
			decorateEvent(consulEvents, pagerEvents)
		}
		_, err = kv.Delete(string(service.Key), nil)
		if err != nil {
			log.Println(err)
		}
	}
}

// If maintainance is set abort any further actions
func maintainance() {
	kv := o.kv
	opts := api.QueryOptions{AllowStale: true}
	pair, _, err := kv.Get("infra/pagerduty/maintainance", &opts)
	check(err)
	if pair == nil || string(pair.Value) == "false" {
		return
	}
	log.Println("Maintainance Mode enable - abording ")
	os.Exit(0)
}

func addToQueue(consulEvents watchEvent) {

	m := make(map[string]string)

	for _, v := range consulEvents {
		m[v.Service.Service] = v.Service.Service
	}

	for _, v := range m {
		storeEvent(v)
	}
}

func assumeHealth() (health bool, version string) {
	operatorInfo, err := o.client.Operator().AutopilotServerHealth(nil)
	if err != nil {
		if err.Error() == "Unexpected response code: 404 ()" {
			health = false
			version = "0.7.5"
			return
		}
		log.Println("Health Fail:", err)
		health = false
		version = "0.0.0"
		return
	}
	health = operatorInfo.Healthy
	version = "1.0.0"
	return
}
