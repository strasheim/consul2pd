package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// https://v2.developer.pagerduty.com/docs/send-an-event-events-api-v2
const endpoint = "https://events.pagerduty.com/v2/enqueue"

// The EventResponse as documented here https://v2.developer.pagerduty.com/docs/send-an-event-events-api-v2
type EventResponse struct {
	Status   string   `json:"status"`
	Message  string   `json:"message"`
	DedupKey string   `json:"dedup_key"`
	Errors   []string `json:"errors"`
}

// The EventRequest as https://v2.developer.pagerduty.com/docs/send-an-event-events-api-v2
type EventRequest struct {
	Payload struct {
		Summary       string              `json:"summary"`
		Timestamp     string              `json:"timestamp,omitempty"`
		Source        string              `json:"source"`
		Severity      string              `json:"severity"`
		Component     string              `json:"component,omitempty"`
		Group         string              `json:"group,omitempty"`
		Class         string              `json:"class,omitempty"`
		CustomDetails map[string][]string `json:"custom_details,omitempty"`
		//CustomDetails map[string]interface{} `json:"custom_details,omitempty"`

	} `json:"payload,omitempty"`
	RoutingKey string `json:"routing_key"`
	DedupKey   string `json:"dedup_key"`
	Images     []struct {
		Src  string `json:"src,omitempty"`
		Href string `json:"href,omitempty"`
		Alt  string `json:"alt,omitempty"`
	} `json:"images,omitempty"`
	Links []struct {
		Href string `json:"href,omitempty"`
		Text string `json:"text,omitempty"`
	} `json:"links,omitempty"`
	EventAction string `json:"event_action"`
	Client      string `json:"client,omitempty"`
	ClientURL   string `json:"client_url,omitempty"`
}

// The Links struct, it's to allow simple addtion of links to the main EvenRequest struct
type Links []struct {
	Href string `json:"href,omitempty"`
	Text string `json:"text,omitempty"`
}

var client *http.Client

// this will only trigger on resubmits where a single instance has that many submissions
// PD limits on 60 requests per minute

func init() {
	client = &http.Client{
		Timeout: time.Second * 10,
	}

}

// To make the function code simpler = signature clone of submit
// Basically does what the error case would do
func (Req *EventRequest) store() (string, string, error) {
	return Req.Payload.Component, Req.Payload.Class, nil
}

// PagerDuty request , no internal retry handling
// Checks for 3 HTTP status codes 202 400 and 429 all others are reported but not handled
func (Req *EventRequest) submit() (string, string, error) {
	body, err := json.Marshal(Req)
	if err != nil {
		return Req.Payload.Component, Req.Payload.Class, err
	}
	//fmt.Println(string(body))
	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(body))
	if err != nil {
		return Req.Payload.Component, Req.Payload.Class, err
	}
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return Req.Payload.Component, Req.Payload.Class, err
	}
	defer resp.Body.Close()
	if resp.StatusCode == 202 || resp.StatusCode == 400 {
		rawContent, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return Req.Payload.Component, Req.Payload.Class, err
		}
		data := &EventResponse{}
		err = json.Unmarshal(rawContent, data)
		if err != nil {
			return Req.Payload.Component, Req.Payload.Class, err
		}
		if resp.StatusCode == 202 {
			return Req.Payload.Component, Req.Payload.Class, nil
		}
		allErrors := errors.New(strings.Join(data.Errors, "\n"))
		return data.Status, Req.Payload.Class, allErrors
	} else if resp.StatusCode == 429 {
		return Req.Payload.Component, Req.Payload.Class, errors.New("Trottled: retry submission later")
	}
	return "", "", errors.New("Unknown Error - no retry handling")
}

// PagerEvent creates the base EventRequest structure
// Returns the EventRequest object with can be futher customized
func PagerEvent(routingKey string, dedupKey string, eventAction string) *EventRequest {
	return &EventRequest{RoutingKey: routingKey,
		EventAction: eventAction,
		DedupKey:    dedupKey}
}
